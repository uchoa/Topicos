#ifndef Project4_SG_Main
#define Project4_SG_Main
#include <SDL/SDL.h>
#include <vector>
#include "Mesh.h"
#include <string>
namespace uni {
	class  SG
	{
	public:
		 SG( unsigned w, unsigned h);
		virtual ~ SG();

	private:
		void start();
		void mainLoop();
		void checkEvent(SDL_Event&);
		void draw();
        void VBO();
        void load_obj(const char* file);
        void drawVBO();
        void seletor();
        void SeletorUpdate();
        void vboUpdate();
        void drawVBOSe();
        void vboUp();
	private:
		unsigned _w, _h;
		bool _done;
		float _y;
		float _x;
		float _ang;
		int idx;
        unsigned  _indexVB;
        unsigned  _indexS;
        unsigned  _indesVBOU;
        unsigned  _seletorVB;
	    unsigned  _vertexVB;
	    unsigned  _indexSize;
        int seletorId;
        unsigned indexUp[21];
	    Mesh mesh;
        std::vector<int> index;
        std::vector<int> indexS;
        std::vector<uni::Vec3> pos;
        std::vector<float> post;
        std::vector<float> posUp;
        std::vector<float> seletort;
        std::vector<uni::Vec3>normal;
	};


}// end namespace

#endif



