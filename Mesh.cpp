#include "Mesh.h"

namespace uni{



unsigned Mesh::addVertex(Vertex vertex1){
    int indx=vertexSize();
    for(int i=0;i<vertexSize();i++){
        if(getVertex(i).getPos().x()==vertex1.getPos().x()
           &&getVertex(i).getPos().y()==vertex1.getPos().y()
           &&getVertex(i).getPos().z()==vertex1.getPos().z()){
            return i;
        }
    }

   vertices.push_back(vertex1);
    return indx;

}
bool Mesh::conect(unsigned int indx1, unsigned int indx2, unsigned int indx3 ){
    Face face(indx1,indx2,indx3);

        faces.push_back(face);
        return true;

}
unsigned Mesh::vertexSize(){
    return vertices.size();
}
unsigned Mesh::faceSize(){
    return faces.size();
}
Face Mesh::getFace(unsigned indx){
   return faces.at(indx);
}
Vertex Mesh::getVertex(unsigned indx){
   return vertices.at(indx);
}

Mesh::~Mesh(){}
}//end namespace
