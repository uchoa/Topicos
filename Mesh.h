#ifndef UNI_MESH_H_
#define UNI_MESH_H_
#include "Vec3.h"
#include "Vertex.h"
#include "Face.h"
#include <vector>

namespace uni {
class Mesh{
public:
    virtual ~Mesh();
    unsigned addVertex(Vertex vertex1);
    bool conect(unsigned int indx1, unsigned int indx2, unsigned int indx3);
    unsigned int vertexSize();
    unsigned int faceSize();
    Vertex getVertex( unsigned int indx);
    Face getFace(unsigned int indx);

private:
   std::vector<Face>faces;
   std::vector<Vertex>vertices;
};
} //END namespace

#endif
