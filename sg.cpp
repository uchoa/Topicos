#include "sg.h"
#include "init_manager.h"
#include "Mesh.h"
#include "Vec3.h"
#include "Vertex.h"
#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
namespace uni {
	SG::SG(unsigned w, unsigned h)
		:_w(w), _h(h)
	{

	    _x=-5.5f;
        _y=0.f;
        _ang=0;
        seletorId = 0;


		this->start();

        const char* arq= "./cube.obj";
        load_obj(arq);

        this->seletor();
        this->VBO();

		this->mainLoop();
	}
	SG::~SG() {}
	void SG::start() {


		InitManager::initSDLVideo(_w, _h);
		InitManager::initGL();
		InitManager::initView(_w,_h);
		InitManager::initGlew();

	}
	void SG::mainLoop() {
	    SDL_Event e;
		while (!_done)
		{
			this->checkEvent(e);
			this->draw();

		}
	}
	void SG::checkEvent(SDL_Event& e){
        while(SDL_PollEvent(&e)){
            switch (e.type){
                case SDL_QUIT:
                    _done=true; break;
                case SDL_KEYDOWN:
                    switch (e.key.keysym.sym){
                        case SDLK_ESCAPE:

                            _done=true;break;

                        case SDLK_DOWN:
                            //diminui o eixo y
                            _x=1;
                            _y=0;
                            _ang+=15.f;
                            break;
                        case SDLK_UP:
                            //adiciona ao eixo y
                            _x=1;
                            _y=0;
                            _ang-=15.f;
                            break;
                        case SDLK_LEFT:
                            //diminui o eixo x
                            _y=1;
                            _x=0;
                            _ang-=15.f;
                            break;
                         case SDLK_RIGHT:
                             //adiciona ao eixo x
                           _y=1;
                           _x=0;
                           _ang+=15.f;
                            break;
                         case SDLK_r:

                            if(seletorId+1>mesh.faceSize()-1){
                                seletorId=0;
                            }else{
                                seletorId++;
                            }
                            seletor();
                            SeletorUpdate();
                            break;
                        case SDLK_t:

                            if(seletorId-1<0){
                                seletorId=mesh.faceSize()-1;
                            }else{
                                seletorId--;
                            }
                            seletor();
                            SeletorUpdate();
                            break;
                        case SDLK_w:
                            vboUpdate();
                            vboUp();
                            break;
                        default: break;
                    }
                    break;
            }
        }
	}
    void SG::draw(){

        glClearColor(1.f,0.f,1.f,1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glLoadIdentity();
        glPushMatrix();
        glTranslatef(-4.f,0.f,-16.f);
        glRotatef(_ang, _x,_y,0 );
        drawVBO();

        drawVBOSe();
        glPopMatrix();
        SDL_GL_SwapBuffers();

    }

    void SG::VBO(){

         _indexSize=index.size();
        //index da sequencia dos pontos
        glGenBuffers(1,&_indexVB);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_indexVB);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,index.size()*sizeof(unsigned int),index.data(),GL_STATIC_DRAW);
        //posisao dos pontos
        glGenBuffers(1,&_vertexVB);
        glBindBuffer(GL_ARRAY_BUFFER,_vertexVB);
        glBufferData(GL_ARRAY_BUFFER,post.size()*sizeof(float),post.data(),GL_STATIC_DRAW);

        //posisao dos pontos(seletor)
        glGenBuffers(1,&_seletorVB);
        glBindBuffer(GL_ARRAY_BUFFER,_seletorVB);
        glBufferData(GL_ARRAY_BUFFER,seletort.size()*sizeof(float),seletort.data(),GL_STATIC_DRAW);

        glGenBuffers(1,&_indexS);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_indexS);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,indexS.size()*sizeof(unsigned int),indexS.data(),GL_STATIC_DRAW);



        //glDeleteBuffers
    }
    void SG::load_obj(const char* file ){

        std::ifstream in(file, std::ios::in);
        if (!in) { std::cerr << "N�o pode abrir o arquivo: " << file<< std::endl; exit(1); }
        std::string temp;
        float x,y,z;
        int i=0;
        while (getline(in, temp)){
            std::stringstream ss;
            ss<<temp;
            if(temp[0]=='v' && temp[1]==' ' ){
                char temc;
                ss>>temc>>x>>y>>z;
                Vec3 vec(x,y,z);
                pos.push_back(vec);
                post.push_back(x);
                post.push_back(y);
                post.push_back(z);

            }else if(temp[0]=='v' && temp[1]=='n' ){
                char temc;
                ss>>temc>>x>>y>>z;
                Vec3 nor(x,y,z);

                normal.push_back(nor);
            }else if(temp[0]=='f' && temp[1]==' ' ){
                char temc;
                int v1,v2,v3,n1,n2,n3;

                ss>>temc>>v1>>temc>>temc>>n1>>v2>>temc>>temc>>n2>>v3>>temc>>temc>>n3;

                Vertex vec1(pos.at(v1-1),normal.at(n1-1));
                Vertex vec2(pos.at(v2-1),normal.at(n2-1));
                Vertex vec3(pos.at(v3-1),normal.at(n3-1));

                index.push_back(v1-1);
                index.push_back(v2-1);
                index.push_back(v3-1);

               if( mesh.conect(mesh.addVertex(vec1), mesh.addVertex(vec2), mesh.addVertex(vec3))){

               }

            }
        }

    }
     void SG::seletor(){

        std::cout<<seletorId<<std::endl;
        seletort.clear();
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v1()).getPos().x() );
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v1()).getPos().y() );
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v1()).getPos().z() );

        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v2()).getPos().x() );
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v2()).getPos().y() );
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v2()).getPos().z() );

        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v3()).getPos().x() );
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v3()).getPos().y());
        seletort.push_back(mesh.getVertex(mesh.getFace(seletorId).get_v3()).getPos().z() );

        indexS.clear();
        indexS.push_back(0);
        indexS.push_back(1);
        indexS.push_back(2);
     }
     void SG::drawVBO(){

        glBindBuffer(GL_ARRAY_BUFFER,_vertexVB);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,0,0);
        glEnableClientState(GL_ELEMENT_ARRAY_BUFFER);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_indexVB );
        glColor3f(1,1,0);
        glDrawElements(GL_TRIANGLES,_indexSize,GL_UNSIGNED_INT, 0);

        glDisableClientState(GL_ELEMENT_ARRAY_BUFFER);
        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void SG::drawVBOSe(){
        glBindBuffer(GL_ARRAY_BUFFER,_seletorVB);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,0,0);
        glEnableClientState(GL_ELEMENT_ARRAY_BUFFER);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_indexS );

        glColor3f(1,0,0);
        glLineWidth(5);
        glDrawElements(GL_LINE_LOOP,3,GL_UNSIGNED_INT, 0);


        glDisableClientState(GL_ELEMENT_ARRAY_BUFFER);
        glDisableClientState(GL_VERTEX_ARRAY);


    }

    void SG::SeletorUpdate(){
    glBindBuffer(GL_ARRAY_BUFFER, _seletorVB);
    memcpy(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY), (float*) seletort.data(), seletort.size() * sizeof(float));
    glUnmapBuffer(GL_ARRAY_BUFFER);
}
    void SG::vboUpdate(){
    posUp.clear();
    Vec3 no1=mesh.getVertex(mesh.getFace(seletorId).get_v1()).getPos();
    Vec3 no2= mesh.getVertex(mesh.getFace(seletorId).get_v2()).getPos();
    Vec3 no3=mesh.getVertex(mesh.getFace(seletorId).get_v3()).getPos();

    Vec3 aux1=no2-no1;
    Vec3 aux2=no3-no1;
    Vec3 normal1=aux1.cross(aux2).normalize();

    no1=no1+normal1;
    no2=no2+normal1;
    no3=no3+normal1;

    Vertex vex1(no1,no1);
    Vertex vex2(no2,no2);
    Vertex vex3(no3,no3);

    mesh.conect(mesh.addVertex(vex1),mesh.addVertex(vex2),mesh.addVertex(vex3));

    posUp.push_back(vex1.getPos().x());
    posUp.push_back(vex1.getPos().y());
    posUp.push_back(vex1.getPos().z());

    posUp.push_back(vex2.getPos().x());
    posUp.push_back(vex2.getPos().y());
    posUp.push_back(vex2.getPos().z());

    posUp.push_back(vex3.getPos().x());
    posUp.push_back(vex3.getPos().y());
    posUp.push_back(vex3.getPos().z());

    Face faceaux= mesh.getFace(mesh.faceSize()-1);
    int pri= faceaux.get_v1();
    int seg=faceaux.get_v2();
    int ter=faceaux.get_v3();

    Face faceWhereTheSelectorIsCurrentOn (mesh.getFace(seletorId).get_v1(),mesh.getFace(seletorId).get_v2(),mesh.getFace(seletorId).get_v3());

    int priCurrent = faceWhereTheSelectorIsCurrentOn.get_v1();
    int segCurrent = faceWhereTheSelectorIsCurrentOn.get_v2();
    int terCurrent = faceWhereTheSelectorIsCurrentOn.get_v3();


    indexUp={priCurrent,segCurrent,pri,
             segCurrent,pri,terCurrent,
             segCurrent,seg,pri,
             terCurrent,pri,ter,
             priCurrent,terCurrent,seg,
             priCurrent,seg,ter,
             seg,pri,ter
                };

}
    void SG::vboUp(){
        int bufferSize;

        glGenBuffers(1,&_indesVBOU);
        glBindBuffer(GL_COPY_READ_BUFFER,_indexVB);
        glGetBufferParameteriv(GL_COPY_READ_BUFFER,GL_BUFFER_SIZE,&bufferSize);

        glBindBuffer(GL_COPY_WRITE_BUFFER, _indesVBOU);
        glBufferData(GL_COPY_WRITE_BUFFER, bufferSize+(21*sizeof(unsigned)), 0, GL_STATIC_DRAW);
        glCopyBufferSubData(GL_COPY_READ_BUFFER,GL_COPY_WRITE_BUFFER,0, 0, bufferSize);

        glDeleteBuffers(1,&_indexVB);
        _indexVB=_indesVBOU;
        glBindBuffer(GL_ARRAY_BUFFER,_indexVB);
        glBufferSubData(GL_ARRAY_BUFFER,bufferSize,21*sizeof(unsigned),indexUp);
        _indexSize+=21;

        glGenBuffers(1,&_indesVBOU);
        glBindBuffer(GL_COPY_READ_BUFFER,_vertexVB);
        glGetBufferParameteriv(GL_COPY_READ_BUFFER,GL_BUFFER_SIZE,&bufferSize);

        glBindBuffer(GL_COPY_WRITE_BUFFER, _indesVBOU);
        glBufferData(GL_COPY_WRITE_BUFFER, bufferSize+(9*sizeof(float)), 0, GL_STATIC_DRAW);
        glCopyBufferSubData(GL_COPY_READ_BUFFER,GL_COPY_WRITE_BUFFER,0, 0, bufferSize);

        glDeleteBuffers(1,&_vertexVB);
        _vertexVB=_indesVBOU;
         glBindBuffer(GL_ARRAY_BUFFER,_vertexVB);
        glBufferSubData(GL_ARRAY_BUFFER,bufferSize,9*sizeof(float),posUp.data());


}
}//end namespace



