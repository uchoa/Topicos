#ifndef UNI_SELECTOR_RENDER_H_
#define UNI_SELECTOR_RENDER_H_

#include "render.h"
#include "vertex.h"

namespace uni {

struct Selector {
    Vertex v[3];
    unsigned i[3];
};

class SelectorRender : public Render {
public:
    SelectorRender(const Selector& );
    virtual ~SelectorRender();
    void recreate(const Selector& );
};

} //END namespace

#endif
