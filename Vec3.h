#ifndef UNI_VEC3_H_
#define UNI_VEC3_H_
namespace uni{
class Vec3{
    public:
    Vec3(float x = 0.f, float y = 0.f, float z = 0.f);
    virtual ~Vec3();

    void x(float x);
	void y(float y);
	void z(float z);

	float x() const;
	float y() const;
	float z() const;

    Vec3 operator*(float scalar) const;
    Vec3 operator+(const Vec3& other) const;
    Vec3 operator-(const Vec3& other) const;
    Vec3 cross(const Vec3& other) const;
    Vec3 normalize() const;
    private:
    float _x, _y, _z;
};
}
#endif
