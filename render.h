#ifndef UNI_RENDER_H_
#define UNI_RENDER_H_

namespace uni {

class Render {
public:
    enum RenderModeE {TRIANGLE_MODE, LINE_MODE, POINT_MODE, RENDER_MODE_SIZE};
public:
    Render();
    virtual ~Render();

    virtual void draw();

protected:
    void initPosition(float* position, unsigned arraySize);
    void rePosition(float* position, unsigned arraySize);
    void initIndices(unsigned *indices, unsigned indicesSize);

    void pushData(unsigned size, unsigned& vboIdx, float* data);
    void pushIndex(unsigned size, unsigned* data);

protected:
    unsigned _indicesSize;
    RenderModeE _renderMode;
    unsigned _positionVB, _indiceVB;

private:
    static const unsigned _renderModes[3];
};

} //END namespace

#endif

