#ifndef UNI_VERTEX_H_
#define UNI_VERTEX_H_
#include "Vec3.h"

namespace uni {
class Vertex{
public:
    Vertex(Vec3 pos, Vec3 normal);
    virtual ~Vertex();
    Vec3 getPos();
    Vec3 getNorma();



private:
    Vec3 pos;
    Vec3 normal;
};

} //END namespace

#endif
