#include "Vec3.h"
#include <cmath>
#include <iostream>

namespace uni {
Vec3::Vec3(float x, float y,float z):_x(x),_y(y),_z(z){

}
Vec3::~Vec3() {}
void Vec3::x(float x) { _x = x; }
void Vec3::y(float y) { _y = y; }
void Vec3::z(float z) { _z = z; }

float Vec3::x() const { return _x; }

float Vec3::y() const { return _y; }

float Vec3::z() const { return _z; }

Vec3 Vec3::operator*(float scalar) const {
    return Vec3(_x * scalar, _y * scalar, _z * scalar);
}

Vec3 Vec3::operator+(const Vec3 &other) const {
    return Vec3(
                _x + other.x(),
                _y + other.y(),
                _z + other.z()

                );
}

Vec3 Vec3::operator-(const Vec3 &other) const {
    return Vec3(

                _x - other.x(),
                _y - other.y(),
                _z - other.z()

                );
}

Vec3 Vec3::cross(const Vec3 &other) const {

    return Vec3(
                _y * other.z() - _z * other.y(),
                _z * other.x() - _x * other.z(),
                _x * other.y() - _y * other.x()

                );
}

Vec3 Vec3::normalize() const {
    float module = fabs(sqrt(_x * _x + _y * _y + _z * _z));
    return Vec3(
                    _x / module,
                    _y / module,
                    _z / module
                );
}
} //END namespace
