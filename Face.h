#ifndef UNI_FACE_H_
#define UNI_FACE_H_
#include "Vec3.h"
namespace uni {
class Face {
public:
    Face(unsigned  v1, unsigned  v2, unsigned  v3);
    virtual ~Face();
    unsigned  get_v1();
    unsigned  get_v2();
    unsigned  get_v3();


private:
    unsigned  _v1,_v2,_v3;

};
}//end namespace

#endif
